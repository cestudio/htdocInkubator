<?php
 require_once('dbConnect.php');


 $sql = "SELECT * FROM (SELECT * FROM data_sensor t1 INNER JOIN ( SELECT MAX(idsensor) AS ID, MAX(tanggal) AS MAXDATE FROM data_sensor GROUP BY idinkubator ) t2 ON t1.idsensor = t2.ID AND t1.tanggal = t2.MAXDATE) as a inner join data_inkubator on a.idinkubator=data_inkubator.idinkubator inner join data_bayi on data_inkubator.idinkubator = data_bayi.idinkubator";

 $res = mysqli_query($con,$sql);

 $result = array();

 while($row = mysqli_fetch_array($res)){

	array_push($result,array('idinkubator'=>$row['idinkubator'],'namainkubator'=>$row['nama_inkubator'],'namabayi'=>$row['nama_bayi'],'detakjantung'=>$row['detak_jantung'],'kadaroksigen'=>$row['kadar_oksigen'],'beratbadan'=>$row['berat_badan'],'suhu'=>$row['suhu'],'kelembaban'=>$row['kelembaban'],'tanggal'=>$row['tanggal']));
 }

 echo json_encode(array("result"=>$result));

 mysqli_close($con);

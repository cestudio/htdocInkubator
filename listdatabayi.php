<?php
 require_once('dbConnect.php');
 
 $sql = "SELECT * FROM data_bayi inner join data_inkubator on data_bayi.idinkubator = data_inkubator.idinkubator";
 
 $res = mysqli_query($con,$sql);
 
 $result = array();
 
 while($row = mysqli_fetch_array($res)){
	
	array_push($result,array('idbayi'=>$row['idbayi'],'namabayi'=>$row['nama_bayi'],'beratbadan'=>$row['berat_badan']." Kg",'idinkubator'=>$row['idinkubator']));
 }
 
 echo json_encode(array("result"=>$result));
 
 mysqli_close($con);